function [T_sys, dT_sys] = transmittanceStat(N, Nsmp, Ngrp, T)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
%
%   N
%       Number of boundaries in the system.
%   Nsmp
%       Number of experiment in each independent group. Total number of
%       experiments is (Nsmp * Ngrp).
%   Ngrp
%       Number of indpenedent group of experiments, for estimation of
%       stddev.
%   T
%       Transmittance of a single boundary.

%% initialize
% mean of the bernoulli distributed random array is_transmitted.
T_sys = zeros(numel(N),numel(T));
% stddev of the bernoulli distributed random array is_transmitted.
dT_sys = zeros(numel(N),numel(T));

is_transmitted = false(Nsmp, Ngrp);

%% iterate
NT = numel(T);
tic;

% iterate over the grid of N x T
for Ti = 1:numel(T)
    t = T(Ti);
    for Ni = 1:numel(N)
        n = N(Ni);
        
        % make Nsmp*Ngrp independent experiments; this can be run in
        % parallel, since the experiments are independent
        parfor k = 1:numel(is_transmitted);
            is_transmitted(k) = transmittance(n, t);
        end
        
        % estimate transmittance as fraction of samples that were
        % transmitted
        T_sys(Ni, Ti) = mean(is_transmitted(:));
        % estimate error as stddev/sqrt(N-2)
        dT_sys(Ni, Ti) = std(mean(is_transmitted)) / sqrt(Ngrp - 2);
    end
    
    % debugging, show progress
    disp(['transmittanceStats, T = ', num2str(t), ...
        '; ', int2str(Ti), '/', int2str(NT)]);
    toc;
end
end