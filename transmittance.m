function [is_transmitted] = transmittance(N, T)
%TRANSMITTANCE Returns 1 with probability T*, the total transmittance of 
%a system with N independent boundaries, each with transmittance T.
%   A particle is stepping through N steps, and trasmitted or reflected
%   with probailities of T or (1-T), respectively. If the particle gets to
%   the other side of the system and exits, it's considered as transmitted
%   through thre system and the method returns 1. Otherwise, the method
%   returns 0.
%
%   N
%       Number of boundaries in the system.
%   T
%       Transmittance of a single boundary.
%   

% the particle step initiated as beginning
step = 1;
% particle direction initiated as forward
momentum = 1;

while true
    % if particle exits the system, stop 
    if step == 1 && momentum == -1
        is_transmitted = false;
        return;
    elseif step == N+1
        is_transmitted = true;
        return;
    end
    
    % continue with the current direction with prob. T (transittance
    % through a single boundary
    if rand() <= T
        step = step + momentum;
    % with prob. (1-T), change the direction (reflection).
    else
        momentum = -momentum;
    end
end
end

