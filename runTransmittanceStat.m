%%
sys = struct();
% test cases of 0,...,100 boundaries
sys.N = (0:100)';
% test cases of 70 T values, exponently distributed from 1 to 0
sys.T = 1 - exp(linspace(0, -6, 70))';
% 10,000 experiments for each (N, T) pair, divided to 5 groups for
% estmiation of stddev
sys.Nsmp = 2000;
sys.Ngrp = 5;
% run the experiments
[sys.Ttot, sys.dTtot] = transmittanceStat(sys.N, sys.Nsmp, sys.Ngrp, sys.T);

%% get p1, q1
NT = numel(sys.T);
sys.p = zeros(NT, 1);
sys.dp = zeros(NT, 1);
sys.q = zeros(NT, 1);
sys.dq = zeros(NT, 1);
pi = 1;
qi = 2;

for i = 1:NT
    fits = fit(sys.N, sys.Ttot(:,i), 'rat01', 'Robust', 'LAR', 'Weights', 1./(sys.dTtot(:,i)+1e-3), 'Lower', [0, 0], ...
        'StartPoint', [1, 1]);
    vals = coeffvalues(fits);
    conf = confint(fits);
    sys.p(i) = vals(pi);
    sys.q(i) = vals(qi);
    sys.dp(i) = (conf(2,pi) - conf(1,pi)) / 2;
    sys.dq(i) = (conf(2,qi) - conf(1,qi)) / 2;
    
    % debugging
    disp(['fit, T = ', num2str(sys.T(i)), ...
        '; ', int2str(i), '/', int2str(NT)]);
end;
clear pi qi NT i fits vals conf
sys.fits = table(sys.T, sys.p, sys.q, sys.dp, sys.dq);